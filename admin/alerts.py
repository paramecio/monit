from settings import config
from flask import g, url_for, request
from paramecio2.libraries.generate_admin_class import GenerateAdminClass
from paramecio2.libraries.i18n import I18n
from paramecio2.modules.admin import admin_app, t as admin_t
from paramecio2.libraries.db.coreforms import SelectForm
from copy import copy
from paramecio2.libraries.mtemplates import env_theme
import os
from paramecio2.libraries.mtemplates import PTemplate, env_theme
from paramecio2.libraries.db import coreforms
from settings import config
#from datetime import datetime
from paramecio2.libraries import datetime
from paramecio2.libraries.urls import make_media_url
from modules.monit.models.monit import Server, ServerData, Alerts
from paramecio2.libraries.db.webmodel import WebModel
from paramecio2.libraries.lists import AjaxList
try:
    import ujson as json
except:
    import json
    
from modules.monit.libraries.alerts import pre_alerts, get_alerts_config

#t=copy(admin_t)

#t.env.directories.insert(0, os.path.dirname(__file__).replace('/admin', '')+'/templates')

env=env_theme(__file__)

t=PTemplate(env)

t.env.directories=admin_t.env.directories

tpl_path=os.path.dirname(__file__).replace('/admin', '')+'/templates/admin'

if t.env.directories[1]!=tpl_path:
    t.env.directories.insert(1, tpl_path)

get_alerts_config()

@admin_app.route('/monit/alerts')
def monit_alerts():
    
    return t.load_template('alerts.phtml', title=I18n.lang('monit', 'alerts', 'Alerts'), contents="", path_module='admin_app.monit_alerts', pre_alerts=pre_alerts)
    

@admin_app.route('/monit/add_alert', methods=['POST'])
def monit_add_alert():
    
    db=g.connection
    
    error=1
    
    txt_error=''
    
    alert=Alerts(db)
    
    alert.create_forms()
    
    type_alert=request.form.get('type_alert', '').strip()
    name=request.form.get('name', '').strip()
    webhook=request.form.get('webhook', '').strip()
    
    num_repeat=request.form.get('num_repeat', '0').strip()
    
    error_form={}
    
    if alert.insert({'type_alert': type_alert, 'name': name, 'webhook': webhook, 'num_repeat': num_repeat}):
        error=0
    else:
        error_form['#error_alert']='Error: cannot create the new alert'
        txt_error=str(alert.show_errors())
        print(txt_error)
        
    return {'error': error, 'txt_error': txt_error, 'error_form': error_form, 'type_alert': type_alert}
    

@admin_app.route('/monit/get_alerts/', methods=['POST'])
def monit_get_alerts():

    db=g.connection
    
    fields=[[I18n.lang('monit', 'name', 'Name'), True], [I18n.lang('monit', 'webhook', 'Webhook'), False], [I18n.lang('monit', 'num_repeat', 'Number of warnings sended'), False],  [I18n.lang('monit', 'options', 'Options'), False]]
    arr_order_fields=['type_alert', 'webhook']    
    
    count_query=['select count(alerts.id) as num_elements from alerts', []]
    
    # server.id as select_id,
    
    # select hostname, ip, date, num_updates, id from serverofuser where user_id=%s;
    
    str_query=['select alerts.name, alerts.webhook, num_repeat, alerts.id as options from alerts', []]
    
    ajax=AjaxList(db, fields, arr_order_fields, count_query, str_query)
    
    """
    ajax.func_fields['id']=options_server
    ajax.func_fields['date']=options_status
    ajax.func_fields['ip']=options_ip
    ajax.func_fields['data']=options_data
    
    
    """
    
    ajax.func_fields['num_repeat']=options_repeat
    
    ajax.func_fields['options']=options_options
    ajax.limit=0
    
    #{'fields': [['Hostname', True], ['IP', True], ['Status', True], ['Options', False]], 'rows': [{'hostname': 'debian-pc.localdomain', 'ip': '<span id="ip_192.168.122.125">192.168.122.125</span>', 'date': '<img src="/mediafrom/monit/images/status_green.png" />', 'id': '<a href="#">View stats</a>'}, {'hostname': 'DESKTOP-HLHPSSO', 'ip': '<span id="ip_192.168.122.81">192.168.122.81</span>', 'date': '<img src="/mediafrom/monit/images/status_green.png" />', 'id': '<a href="#">View stats</a>'}], 'html_pages': ''}
    
    return ajax.show()

@admin_app.route('/monit/delete_alert/<int:alert_id>', methods=['GET'])
def monit_delete_alert(alert_id):
    
    error=0
    
    txt_error=''
    
    db=g.connection
    
    alert=Alerts(db)
    
    alert.set_conditions('WHERE id=%s', [alert_id]).delete()
    
    return {'error': error, 'txt_error': txt_error}

def options_repeat(row_id, row):
    
    if row['num_repeat']==0:
        return ''
        
    return row['num_repeat']

def options_options(row_id, row):
    
    return '<a href="'+url_for('admin_app.monit_delete_alert', alert_id=row_id)+'" class="delete_alert">'+I18n.lang('monit', 'delete', 'Delete')+'</a>'
    
    
