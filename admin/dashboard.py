from settings import config
from flask import g, url_for, request
from paramecio2.libraries.generate_admin_class import GenerateAdminClass
from paramecio2.libraries.i18n import I18n
from paramecio2.modules.admin import admin_app, t as admin_t
from paramecio2.libraries.db.coreforms import SelectForm
from copy import copy
from paramecio2.libraries.mtemplates import env_theme
import os
from paramecio2.libraries.mtemplates import PTemplate, env_theme
from paramecio2.libraries.db import coreforms
from settings import config
#from bson.son import SON
#from datetime import datetime
from paramecio2.libraries import datetime
from paramecio2.libraries.urls import make_media_url
from modules.monit.models.monit import Server, ServerData, Alerts
from paramecio2.libraries.db.webmodel import WebModel
from paramecio2.libraries.lists import AjaxList
try:
    import ujson as json
except:
    import json
    
env=env_theme(__file__)

t=PTemplate(env)

t.env.directories=admin_t.env.directories

tpl_path=os.path.dirname(__file__).replace('/admin', '')+'/templates/admin'

if t.env.directories[1]!=tpl_path:
    t.env.directories.insert(1, tpl_path)

@admin_app.route('/monit/dashboard/')
def monit_dashboard():
    
    return t.load_template('dash_monit.phtml', title=I18n.lang('monit', 'dashboard', 'Dashboard'), path_module='admin_app.monit_dashboard')

@admin_app.route('/monit/get_monit_dashboard/')
def get_monit_dashboard():
    
    db=g.connection
    
    error=0
    num_servers=0
    
    with db.query('select count(*) as num_servers from server') as cursor:
        arr_count=cursor.fetchone()
        if arr_count:
            num_servers=arr_count['num_servers']
        
    timestamp=datetime.obtain_timestamp(datetime.now(utc=True))-120
    
    date_last=datetime.timestamp_to_datetime(timestamp, sql_format_time='YYYY-MM-DD HH:mm:ss')
    
    num_down=0
    
    with db.query('select count(*) as num_servers from serverdata WHERE serverdata.id IN (select MAX(id) from serverdata group by ip, serverdata.group) AND serverdata.date>%s', [date_last]) as cursor:
        arr_count=cursor.fetchone()
        if arr_count:
            num_down=arr_count['num_servers']
    
    num_down=num_servers-num_down

    #select AVG(JSON_EXTRACT(serverdata.data, '$.cpu_idle')) as cpu_idle_medium from serverdata WHERE serverdata.id IN (select MAX(id) from serverdata group by ip)
    
    avg_cpu=0;
    
    with db.query('select AVG(JSON_EXTRACT(serverdata.data, \'$.cpu_idle\')) as cpu_idle_avg from serverdata WHERE serverdata.id IN (select MAX(id) from serverdata group by ip, serverdata.group) AND serverdata.date>%s', [date_last]) as cursor:
        arr_medium=cursor.fetchone()
        if arr_medium:
            avg_cpu=arr_medium['cpu_idle_avg']

    # Network
    
    now=datetime.obtain_timestamp(datetime.now(utc=True))
            
    hours12=now-3660
    
    date_now=datetime.timestamp_to_datetime(now, sql_format_time='YYYY-MM-DD HH:mm:00')
    
    date_hours12=datetime.timestamp_to_datetime(hours12, sql_format_time='YYYY-MM-DD HH:mm:00')
    
    #print(date_hours12+' '+date_now)
    
    first_sent={}
    first_recv={}
    first_time={}
    
    arr_net={}
    
    with db.query('select JSON_EXTRACT(serverdata.data, \'$.net_info[0]\') as bytes_send, JSON_EXTRACT(serverdata.data, \'$.net_info[1]\') as bytes_recv, serverdata.ip, serverdata.date from serverdata WHERE date>=%s AND date<=%s order by serverdata.date ASC', [date_hours12, date_now]) as cursor:
        for row in cursor:
            
            #if first_sent==0 and first_recv==0:
            if not row['ip'] in first_time:
                first_sent[row['ip']]=int(row['bytes_send'])
                first_recv[row['ip']]=int(row['bytes_recv'])
                first_time[row['ip']]=True
            else:
                
                row['date']=str(row['date']).replace('-', '').replace(':', '').replace(' ', '')
                
                date_actual=datetime.format_local_strtime('YYYY-MM-DD HH:mm:ss', datetime.gmt_to_local(row['date']))
                
                if not date_actual in arr_net:
                    
                    arr_net[date_actual]={'send': [], 'recv': []}
                
                bytes_sent=round((int(row['bytes_send'])-first_sent[row['ip']])/1024)
                bytes_recv=round((int(row['bytes_recv'])-first_recv[row['ip']])/1024)
                
                arr_net[date_actual]['send'].append(bytes_sent)
                arr_net[date_actual]['recv'].append(bytes_recv)
                
                first_sent[row['ip']]=int(row['bytes_send'])
                first_recv[row['ip']]=int(row['bytes_recv'])
                    
    
    arr_loaded_cpu={}
                    
    with db.query('select JSON_EXTRACT(serverdata.data, \'$.cpu_idle\') as cpu_idle, server.hostname from serverdata, server WHERE serverdata.id IN (select MAX(id) from serverdata group by ip, serverdata.group) AND serverdata.date>%s AND serverdata.ip=server.ip AND serverdata.group=server.group order by cpu_idle DESC limit 10', [date_last]) as cursor:
        """
        arr_medium=cursor.fetchone()
        if arr_medium:
            avg_cpu=arr_medium['cpu_idle_avg']
        """
        for row in cursor:
            arr_loaded_cpu[row['hostname']]=row['cpu_idle']

    return {'error': error, 'num_servers': num_servers, 'num_down': num_down, 'avg_cpu': avg_cpu, 'net_info': arr_net, 'loaded_cpu': arr_loaded_cpu}

