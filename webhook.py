from modules.monit import monit_app, db
from flask import request, g
from settings import config
#from pymongo import MongoClient
from modules.monit.models.monit import Server, ServerData
#import datetime
from paramecio2.libraries import datetime
from paramecio2.libraries import sendmail
try:
    import ujson as json
    
except:
    
    import json

send=sendmail.SendMail(ssl=False)

if hasattr(config, 'email_from'):
    email_from=config.email_from

if hasattr(config, 'email_to'):
    email_to=config.email_to

if hasattr(config, 'email_user'):
    send.username=config.email_user

if hasattr(config, 'email_password'):
    send.password=config.email_password
    
if hasattr(config, 'email_host'):
    send.host=config.email_host

if hasattr(config, 'email_port'):
    send.port=config.email_port

@monit_app.route('/alerts/<api_key>', methods=['POST'])
def monit_webhook_alerts(api_key):
    
    #type_alert=request.form.get('type_alert')
    #data=request.form.get('data')
    
    error=1
    txt_error='Error: api key incorrect'
    
    if config.monit_api_key==api_key:
        error=0
        txt_error=''
        
    if error==0:
    
        data=request.get_json()
        
        subject='Alerta de servidor'
        
        #pre_alerts={'server_alert': ('Down server alert', 'modules.monit.alerts.servers'), 'cpu_alert': ('High CPU alert', 'modules.monit.alerts.cpu'), 'disk_alert': ('High Disk use', 'modules.monit.alerts.disks'), 'mem_alert': ('High mem alert', 'modules.monit.alerts.mem')}
        
        if data['type_alert']=='server_alert':
            
            # Set text email
            
            subject+=' Servidor caído'
            
            def text_email(hostname, data):
                
                return 'Alerta: servidor '+hostname+' con ip '+data_host['ip']+' está caído'
                
        elif data['type_alert']=='cpu_alert':
            
            subject+=' Uso de cpu alto'
            
            def text_email(hostname, data):
            
                return 'Alerta: servidor '+hostname+' con ip '+data_host['ip']+' tiene un uso de cpu muy alto '+data_host['cpu_idle']
                
        elif data['type_alert']=='disk_alert':
            
            subject+=' Uso de disco alto'
            #print(data)
            # {'type_alert': 'disk_alert', 'data': {'/': [19372097536, 19355320320, 0, 100.0]}}
            #text_email+="Alerta: servidor "+hostname+" con ip "+data['ip']+" tiene un uso de disco muy alto "+data_host['cpu_idle']+"\n"
            
            def text_email(hostname, data): 
            
                text_email=''
            
                """
                for disk, data_disk in data['data'].items():
                    
                    text_email+="Alerta: servidor "+hostname+" tiene un uso de disco muy alto, disco: "+disk+", porcentaje: "+str(data_disk[3])+"\n"
                """
                #{'fedora': {'hostname': 'fedora', 'ip': '192.168.122.185', 'date': '2022-11-02 16:46:00', 'disks_percent': '{"\\/": [19372097536, 19355320320, 0, 100.0], "\\/boot\\/efi": [535801856, 24576, 535777280, 0.0]}', 'id': 9930}}
                #{'type_alert': 'disk_alert', 'data': {'fedora': {'hostname': 'fedora', 'ip': '192.168.122.185', 'date': '2022-11-02 16:49:00', 'disks_percent': '{"\\/": [19372097536, 19355320320, 0, 100.0], "\\/boot\\/efi": [535801856, 24576, 535777280, 0.0]}', 'id': 9930}}}

                #print(data)
                
                for disk, data_disk in data['data'][hostname]['disks_percent'].items():
                    
                    if data_disk[3]>95:
                    
                        text_email+="Alerta: servidor "+hostname+" tiene un uso de disco muy alto, disco: "+disk+", porcentaje: "+str(data_disk[3])+"\n"
                
                return text_email
        
        arr_msg=[]
        
        for hostname, data_host in data['data'].items():
            
            arr_msg.append(text_email(hostname, data))
            
        
        message="\n\n".join(arr_msg)
        
        if not send.send(email_from, email_to, subject, message, content_type='plain', attachments=[]):
            print(send.txt_error)
        
        send.quit()

        pass
        
    
    return {'error': error, 'txt_error': txt_error}
