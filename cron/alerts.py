import os
import sys

sys.path.insert(0, os.path.realpath(os.path.dirname(__file__))+'/../../../')

from settings import config
from paramecio2.libraries.db.webmodel import WebModel
from modules.monit.models.monit import Alerts
from modules.monit.libraries.alerts import pre_alerts, get_alerts_config
import signal
from time import sleep
import importlib
import requests
try:
    import ujson as json
except:
    import json
from requests.exceptions import Timeout


def start():

    # Load things
    
    db=WebModel.connection()
    
    alerts=Alerts(db)
    
    # Get activate alerts from db. 
    
    repeat_warning={}
    
    db_alerts=alerts.select_to_array()
    
    db.close()
    
    arr_alerts={}
    
    # Get the extra alerts. 

    get_alerts_config()
    
    # Import alerts
    
    for alert, data_alert in pre_alerts.items():
        #print(data_alert)
        arr_alerts[alert]=importlib.import_module(data_alert[1])
    
    # Import all alerts

    while True:
        
        for db_alert in db_alerts:
            
            repeat_warning[db_alert['id']]=repeat_warning.get(db_alert['id'], 0)
            
            #Execute alerts.
            
            alert_check=arr_alerts[db_alert['type_alert']]
            
            result_check, data=alert_check.check_alert()
            
            if result_check:
                
                print('Alert: sending '+db_alert['name']+' to webhook')
                
                try:
                    
                    yes_request=True
                    
                    if db_alert['num_repeat']>0:
                        
                        repeat_warning[db_alert['id']]+=1
                        
                        if repeat_warning[db_alert['id']]>=db_alert['num_repeat']:
                            yes_request=False
                    
                    json_data={'type_alert': db_alert['type_alert'], 'data': data}
                    
                    #data['type_alert']=db_alert['type_alert']
                    
                    if yes_request:
                        r=requests.post(db_alert['webhook'], json=json_data, timeout=5)
                    else:
                        print('Sended all warnings '+str(db_alert['webhook']))
                    
                except Timeout:
                    
                    print('The request timed out')
            
            else:
                
                repeat_warning[db_alert['id']]=0
            
        sleep(60)

if __name__=='__main__':
    
    def catch_signal(sig, frame):
        print('Exiting...')
        exit(0)

    signal.signal(signal.SIGINT, catch_signal)

    start()
