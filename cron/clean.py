import os
import sys

sys.path.insert(0, os.path.realpath(os.path.dirname(__file__))+'/../../../')

from settings import config
from paramecio2.libraries.db.webmodel import WebModel
from modules.monit.models.monit import ServerData, Server
from modules.monit.libraries.alerts import pre_alerts, get_alerts_config
import signal
from time import sleep
import importlib
import requests
try:
    import ujson as json
except:
    import json


def start():

    # Load things
    
    db=WebModel.connection()
    
    sdata=ServerData(db)
    
    server=Server(db)
    
    #num_rows_server=server.select_count()*120
    
    #num_servers*120
    
    #num_rows_delete=sdata.select_count()-num_rows_server
    
    #if num_rows_delete>0:
    #    sdata.set_order({'id': 0}).set_limit([num_rows_delete]).delete()
    
    arr_server=server.select_to_array(['id', 'ip'])
    
    #print(arr_server)
    for server_row in arr_server:
        num_rows_server=server.set_conditions('WHERE id=%s', [server_row['id']]).select_count()*120
        
        num_rows_delete=sdata.set_conditions('WHERE ip=%s', [server_row['ip']]).select_count()-num_rows_server
        
        if num_rows_delete>0:
            sdata.set_conditions('WHERE ip=%s', [server_row['ip']]).set_order({'id': 0}).set_limit([num_rows_delete]).delete()
        
    db.close()
    
if __name__=='__main__':

    start()
