from paramecio2.libraries.db.webmodel import WebModel
from paramecio2.libraries import datetime
import sys
import os

sys.path.insert(0, os.path.realpath(os.path.dirname(__file__))+'/../../../')

from modules.monit.models.monit import Server, ServerData

# Return data of cheking alert, if first element is true, send hook. In servers, send all bad servers to webhook. 

def check_alert():

    db=WebModel.connection()

    #last_timestamp=datetime.obtain_timestamp(str(data['date']).replace('-', '').replace(':', '').replace(' ', ''))
    timestamp=datetime.obtain_timestamp(datetime.now(utc=True))-140
    
    date_last=datetime.timestamp_to_datetime(timestamp, sql_format_time='YYYY-MM-DD HH:mm:ss')

    z=0
    
    arr_server_down={}

    with db.query('select server.hostname, serverdata.ip, serverdata.date, server.id from server, serverdata WHERE serverdata.id IN (select MAX(id) from serverdata group by ip) AND serverdata.ip=server.ip AND serverdata.date<%s group by serverdata.ip', [date_last]) as cursor:
        for data in cursor:
            
            z+=1
            data['date']=str(data['date'])
            arr_server_down[data['hostname']]=data

    ret=False

    if z>0:
        ret=True

    return ret, arr_server_down

if __name__=='__main__':
    
    print(check_alert())
