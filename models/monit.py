from paramecio2.libraries.db.webmodel import WebModel
from paramecio2.libraries.db import corefields
from paramecio2.libraries.db.extrafields.dictfield import DictField
from paramecio2.libraries.db.extrafields.datefield import DateField
from paramecio2.libraries.db.extrafields.datetimefield import DateTimeField
from paramecio2.libraries.db.extrafields.ipfield import IpField
from paramecio2.libraries.db.extrafields.urlfield import UrlField
from paramecio2.libraries.db.extrafields.urlfield import DomainField
from paramecio2.libraries.db.extrafields.dictfield import DictField
from paramecio2.libraries.db.extrafields.jsonfield import JsonValueField
from paramecio2.libraries.db.extrafields.parentfield import ParentField
from paramecio2.libraries.urls import make_media_url
from paramecio2.libraries import datetime
from paramecio2.modules.admin.models.admin import UserAdmin

class LonelyIpField(IpField):
    
    def __init__(self, name, size=64):
        
        super().__init__(name, size)
        
        self.duplicated_ip=False
        
    def check(self, value):
        
        value=super().check(value)
        
        if self.duplicated_ip==True:
            self.txt_error='Error: you have a server with this ip in the database'
            self.error=True
            return value
        
        return value
        
    def show_formatted(self, value):
        
        return '<input id="ip_%s" value="" type="hidden"/>%s' % (value, value)
        
class LastUpdatedField(DateField):        
    
    def __init__(self, name):
        
        super().__init__(name)
        
        self.escape=False
    
    def show_formatted(self, value):
        
        now=datetime.now(True)
        
        timestamp_now=datetime.obtain_timestamp(now)
        
        timestamp_value=datetime.obtain_timestamp(value)

        five_minutes=int(timestamp_now)-300
        
        if timestamp_value<five_minutes:
            
            return '<img src="'+make_media_url('images/status_red.png', 'leviathan')+'" />'
        else:
            return '<img src="'+make_media_url('images/status_green.png', 'leviathan')+'" />'
            
class Server(WebModel):
    
    def __init__(self, connection=None):
        
        super().__init__(connection)
        
        #self.register(corefields.ForeignKeyField('user_id', UserServer(connection)), True)                
        self.register(DomainField('hostname'), True)
        self.register(corefields.CharField('group'), True)
        self.register(LonelyIpField('ip'), True)
        self.fields['ip'].unique=True
        #self.fields['ip'].indexed=True

class ServerData(WebModel):
    
    #{'net_info': [161457, 247318, 2245, 4036, 0, 0, 7, 0], 'cpu_idle': 1.0, 'cpus_idle': [1.0, 1.0], 'cpu_number': 2, 'disks_info': {'/': [20042096640, 1789661184, 17210728448, 9.4]}, 'mem_info': [1034973184, 714801152, 30.9, 172310528, 679616512, 178278400, 70418432, 14938112, 168108032, 3080192, 48095232], 'hostname': 'debian-pc.localdomain'}
    def __init__(self, connection=None):
        
        super().__init__(connection)
        
        #post={'bytes_sent': net_info[0], 'bytes_recv': net_info[1], 'errin': net_info[2], 'errout': net_info[3], 'dropin': net_info[4], 'dropout': net_info[5], 'date': now, 'ip': ip, 'last_updated': 1}
        #Net
        """
        self.register(corefields.FloatField('net_bytes_sent'))
        self.register(corefields.FloatField('net_bytes_recv'))
        self.register(corefields.FloatField('net_errin'))
        self.register(corefields.FloatField('net_errout'))
        self.register(corefields.FloatField('net_dropin'))
        self.register(corefields.FloatField('net_dropout'))
        """
        #Mem
        #post={'total': mem_info[0], 'available': mem_info[1], 'percent': mem_info[2], 'used': mem_info[3], 'free': mem_info[4], 'active': mem_info[5], 'inactive': mem_info[6], 'buffers': mem_info[7], 'cached': mem_info[8], 'shared': mem_info[9], 'date': now, 'ip': ip, 'last_updated': 1}
        """
        self.register(corefields.FloatField('mem_total'))
        self.register(corefields.FloatField('mem_available'))
        self.register(corefields.FloatField('mem_percent'))
        self.register(corefields.FloatField('mem_used'))
        self.register(corefields.FloatField('mem_free'))
        self.register(corefields.FloatField('mem_active'))
        self.register(corefields.FloatField('mem_inactive'))
        self.register(corefields.FloatField('mem_buffers'))
        self.register(corefields.FloatField('mem_cached'))
        
        #CPU
        self.register(corefields.FloatField('cpu_idle'))
        self.register(DictField('cpus_idle', corefields.FloatField('cpus_idle')))
        self.register(corefields.FloatField('cpu_idle'))
        self.register(corefields.IntegerField('cpus_num'))
        """
        #Disks info
        """
        for disk, data in arr_info['disks_info'].items():
                        
            status_disk.set_conditions('where ip=%s and disk=%s', [ip, disk])
            
            method_update({'ip' : ip, 'disk' : disk, 'date' : now, 'size' : data[0], 'used' : data[1], 'free' : data[2], 'percent' : data[3]})
        """
        self.register(DateTimeField('date'), True)
        self.register(corefields.CharField('group'), True)
        #def __init__(self, name, related_table, size=11, required=False, identifier_field='id', named_field="id", select_fields=[]):
        #self.register(corefields.ForeignKeyField('server_id', Server(connection), named_field='hostname'), True)       
        self.register(LonelyIpField('ip'), True)
        self.register(JsonValueField('data'), True)
        
"""
class AlertsType(WebModel):
    
    def __init__(self, connection=None):
        super().__init__(connection)
        
        self.register(corefields.CharField('name'), True)
        self.register(corefields.CharField('type_alert_code'), True)
"""

class Alerts(WebModel):
    
    def __init__(self, connection=None):
        super().__init__(connection)
        
        self.register(corefields.CharField('name'), True)
        self.register(corefields.CharField('type_alert'), True)
        self.register(UrlField('webhook'), True)
        self.register(corefields.IntegerField('num_repeat'))
        #self.register(corefields.IntegerField('total_repeat'))
        

# Alerts plugables

class ServerUser(WebModel):
    
    #self.register(corefields.ForeignKeyField('user_id', UserServer(connection)), True)                
    def __init__(self, connection=None):
        
        super().__init__(connection)
        self.register(corefields.ForeignKeyField('user_id', UserAdmin(connection)), True)                
        self.register(corefields.ForeignKeyField('server_id', Server(connection)), True)       
        
