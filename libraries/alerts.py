# Alert check data json from db ServerData table

# Plugins are installed with pip how normal python modules. 
# In pastafaristats are added in configuration. Not recommended more of 3 o 4 plugins. 

from settings import config

pre_alerts={'server_alert': ('Down server alert', 'modules.monit.alerts.servers'), 'cpu_alert': ('High CPU alert', 'modules.monit.alerts.cpu'), 'disk_alert': ('High Disk use', 'modules.monit.alerts.disks'), 'mem_alert': ('High mem alert', 'modules.monit.alerts.mem')}

def get_alerts_config():
    
    if hasattr(config, 'monit_alerts'):
        pre_alerts+=config.monit_alerts
