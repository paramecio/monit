#!/usr/bin/python3

from flask import Blueprint, g, request, session, redirect, url_for, current_app
from paramecio2.libraries.db.webmodel import WebModel
from settings import config
from functools import wraps

monit_app=Blueprint('monit_app', __name__)


def db(f):
    
    @wraps(f)
    
    def wrapper(*args, **kwds):
        
        g.connection=WebModel.connection()
        
        try:
        
            code=f(*args, **kwds)
        
            g.connection.close()
            
        except:
            
            g.connection.close()
            
            raise
        
        return code
        
    return wrapper

def login_site(f):
    
    @wraps(f)
    
    def wrapper(*args, **kwds):
        
        if not 'login_monit' in session:
            
            return redirect(url_for('.login'))
            
        else:
            
            return f(*args, **kwds)
   
    return wrapper
