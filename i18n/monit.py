#!/usr/bin/env python3

from paramecio2.libraries.i18n import I18n

I18n.l['en-US']=I18n.l.get('en-US', {})

I18n.l['en-US']['monit']=I18n.l['en-US'].get('monit', {})

I18n.l['en-US']['monit']['alerts']='Alerts'

I18n.l['en-US']['monit']['name']='Name'

I18n.l['en-US']['monit']['webhook']='Webhook'

I18n.l['en-US']['monit']['options']='Options'

I18n.l['en-US']['monit']['delete']='Delete'

I18n.l['en-US']['monit']['config']='Configuration'

I18n.l['en-US']['monit']['servers']='Servers'

I18n.l['en-US']['monit']['choose_server']='Choose server'

I18n.l['en-US']['monit']['hostname']='Hostname'

I18n.l['en-US']['monit']['status']='Status'

I18n.l['en-US']['monit']['cpu_use']='CPU use'

I18n.l['en-US']['monit']['services']='Services'

I18n.l['en-US']['monit']['view_stats']='View stats'

I18n.l['en-US']['monit']['edit']='Edit'

I18n.l['en-US']['monit']['stats']='Stats'

I18n.l['en-US']['monit']['edit_server']='Edit server'

I18n.l['en-US']['monit']['monitoring']='Monitoring'

I18n.l['en-US']['monit']['choose_group']='Choose group'

I18n.l['en-US']['monit']['server']='Server'

I18n.l['en-US']['monit']['ip']='IP'

I18n.l['en-US']['monit']['group']='Group'

I18n.l['en-US']['monit']['save']='Save'

I18n.l['en-US']['monit']['success!']='Success!'

I18n.l['en-US']['monit']['add_alerts']='Add alerts'

I18n.l['en-US']['monit']['config_explain']='Config instructions'

I18n.l['es-ES']=I18n.l.get('es-ES', {})

I18n.l['es-ES']['monit']=I18n.l['es-ES'].get('monit', {})

I18n.l['es-ES']['monit']['alerts']='Alerts'

I18n.l['es-ES']['monit']['name']='Name'

I18n.l['es-ES']['monit']['webhook']='Webhook'

I18n.l['es-ES']['monit']['options']='Options'

I18n.l['es-ES']['monit']['delete']='Delete'

I18n.l['es-ES']['monit']['config']='Configuration'

I18n.l['es-ES']['monit']['servers']='Servers'

I18n.l['es-ES']['monit']['choose_server']='Choose server'

I18n.l['es-ES']['monit']['hostname']='Hostname'

I18n.l['es-ES']['monit']['status']='Status'

I18n.l['es-ES']['monit']['cpu_use']='CPU use'

I18n.l['es-ES']['monit']['services']='Services'

I18n.l['es-ES']['monit']['view_stats']='View stats'

I18n.l['es-ES']['monit']['edit']='Edit'

I18n.l['es-ES']['monit']['stats']='Stats'

I18n.l['es-ES']['monit']['edit_server']='Edit server'

I18n.l['es-ES']['monit']['monitoring']='Monitoring'

I18n.l['es-ES']['monit']['choose_group']='Choose group'

I18n.l['es-ES']['monit']['server']='Server'

I18n.l['es-ES']['monit']['ip']='IP'

I18n.l['es-ES']['monit']['group']='Group'

I18n.l['es-ES']['monit']['save']='Save'

I18n.l['es-ES']['monit']['success!']='Success!'

I18n.l['es-ES']['monit']['add_alerts']='Add alerts'

I18n.l['es-ES']['monit']['config_explain']='Config instructions'

